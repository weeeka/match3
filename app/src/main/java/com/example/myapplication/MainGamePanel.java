package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.SurfaceView;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import  android.view.View;
import android.view.SurfaceHolder;
import android.view.MotionEvent;
import java.util.Random;



public class MainGamePanel extends SurfaceView implements SurfaceHolder.Callback
{
    Canvas canvas;
    private MainThread thread;
    private Unit[][] unitsField;
    float posX, posY;
    int x, y, x0, y0, dx, dy;
    boolean isSwap = false, isMoving = false;
    int click = 0;
    Bitmap texture, piece;

    public MainGamePanel(Context context)
    {
        super(context);
        getHolder().addCallback(this);
        setZOrderOnTop(true);

        this.setBackgroundResource(R.drawable.bground);
         texture = BitmapFactory.decodeResource(getResources(), R.drawable.spr);

        CreateGameField(6,6);

        thread = new MainThread(getHolder(), this);
        setFocusable(true);
    }

    public void CreateGameField(int width, int height)
    {
        unitsField = new Unit[width][height];

        //bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.frame);
        //bitmap = Bitmap.createScaledBitmap(bitmap, 115, 120, false);

        Random rand = new Random();

         for (int i = 0; i <6; i++)
        {
            for (int j = 0; j <6; j++)
            {
                unitsField[i][j] = new Unit(64*j, 64*i, j, i, rand.nextInt(4));
            }
        }
    }



    public void Swap(Unit u1, Unit u2)
    {
        int temp = u1.col;
        u1.col = u2.col;
        u2.col = temp;

        temp = u1.row;
        u1.row = u2.row;
        u2.row = temp;

        unitsField[u1.row][u1.col] = u1;
        unitsField[u2.row][u2.col] = u2;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (!isSwap && !isMoving) click++;
            posX = event.getX(); posY = event.getY();
        }

        if (click == 1)
        {
            x0 = (int)posX/65;
            y0 = (int)posY/65;
        }

        if(click==2)
        {
            x = (int)posX/65;
            y = (int)posY/65;
            if (Math.abs(x-x0)+Math.abs(y-y0) == 1)
            {
                Swap(unitsField[y0][x0], unitsField[y][x]);isSwap=true; click=0;
            }
            else click = 1;
        }

        return true;
    }

    void MatchFinding()
    {
        for (int i =0; i <6; i++) {
            for (int j = 0; j < 6; j++) {
                if (unitsField[i][j].kind==unitsField[i+1][j].kind)
                    if (unitsField[i][j].kind==unitsField[i-1][j].kind)
                        for(int n=0; n < 3; n++) unitsField[i+n][j].match++; /////////////////индекс

                if (unitsField[i][j].kind==unitsField[i][j+1].kind)
                    if (unitsField[i][j].kind==unitsField[i][j-1].kind)
                        for(int n=0; n < 3; n++) unitsField[i][j+n].match++;
            }
        }
    }

    public void update() //update
    {
        isMoving = false;

        for (int i = 0; i <6; i++) {
            for (int j = 0; j <6; j++)
            {
                Unit u = unitsField[i][j];
                for (int n = 0; n<4;n++)
                {
                    dx = u.x - u.col*64;
                    dy = u.y - u.row*64;
                    if (dx == 1) u.x-=dx/Math.abs(dx);
                    if (dy == 1) u.y-=dy/Math.abs(dy);
                }
                if (dx == 1 || dy == 1)isMoving = true;
                //unitsField[i][j].update(dx, dy);
            }
        }

    }

    @Override
    protected void onDraw(Canvas canvas) ///render
    {
        for (int i = 0; i <6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                Unit u = unitsField[i][j];
                Bitmap.createBitmap(texture, u.kind*64, 0, 64, 64);
                canvas.drawBitmap(texture, u.x, u.y, null);
                //piece.setX(u.position.x);
                //piece.setY(u.position.y);
                //piece.draw(sb);

            }
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // ¬ этой точке поверхность уже создана и мы можем
        // безопасно запустить игровой цикл в потоке
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        //посылаем потоку команду на закрытие и дожидаемс¤,
        //пока поток не будет закрыт.
        boolean retry = true;
        while (retry) {
            try {
                thread.setRunning(false);
                thread.join();
                //retry = false;
            } catch (InterruptedException e) {
                // пытаемс¤ снова остановить поток thread
                e.printStackTrace();
            }
        }
    }
}
