package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.view.View;
import android.view.WindowManager;
import android.content.Intent;
import android.app.ActionBar;


public class MainActivity extends AppCompatActivity implements OnClickListener{

    Button btnActTwo; //settings
    //TextView tvOut;
    Button infoBtn;
    Button playBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        playBtn = (Button) findViewById(R.id.play_btn);
        playBtn.setOnClickListener(this);

        infoBtn = (Button) findViewById(R.id.info_btn);
        infoBtn.setOnClickListener(this);

        btnActTwo = (Button) findViewById(R.id.settings_btn);
        btnActTwo.setOnClickListener(this);
    }


    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }


    @Override
    public void onClick(View v)
    {
        Intent intent;
        switch (v.getId())
        {
            case R.id.settings_btn:
                intent = new Intent(this, ActiwityTwo.class);
                startActivity(intent);
                break;
            case  R.id.info_btn:
                intent = new Intent(this, InfoActivity.class);
                startActivity(intent);
                break;
            case R.id.play_btn:
                setContentView(new MainGamePanel(this));
                break;
            default:
                break;
        }
        //setContentView(new MainGamePanel(this));
        //Intent intent = new Intent(MainActivity.this, StartingGameActivity.class);
        //startActivity(intent);
    }
}
